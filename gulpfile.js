'use strict';

const gulp = require('gulp'),
    inlineCSS = require('gulp-inline-css'),
    htmlmin = require('gulp-htmlmin'),
    runSequence = require('run-sequence'),
    settings = require('./settings.json');

gulp.task('inlineCSS', function() {
    return gulp.src(settings.paths.html)
        .pipe(inlineCSS(settings.inlineCSS))
        // .pipe(htmlmin(settings.htmlmin))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function (){
    gulp.watch(settings.paths.css, ['inlineCSS']);
    gulp.watch(settings.paths.html, ['inlineCSS']);
});

gulp.task('default', function (callback) {
    runSequence('inlineCSS', 'watch', callback)
});