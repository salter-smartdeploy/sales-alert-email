## SmartDeploy Email Dev

### Getting Started

1. Install [Node.js with npm](https://nodejs.org/en/) and [Gulp.js](http://gulpjs.com/)
2. Open a command line tool and navigate to this directory
3. Run `npm install` to download all the required node modules
4. Run `gulp` to start watching your files for changes, or `gulp thetaskname` to run a single task

### Gulp Tasks

| Task Name  |  Task Purpose |
|---|---|
| `gulp inlineCSS`  |  Create a new html file with all non-media query CSS inlined  |


### Styles

Styles are based off of Foundation's [Ink Templates](https://foundation.zurb.com/emails/email-templates.html)

Some styles like *Media Queries and hover/focus states need to go in the src/html files*.  
The CSS inliner task can't inject them if they come from a linked CSS file. See this [inliner media query issue](https://github.com/jonkemp/gulp-inline-css/issues/23).

#### How CSS is Inlined

If you have two conflicting styles in app.css, gulp inlineCSS will only write one of them into dist/html.

So if you have this:

```css
.red-text { color: red }
.blue-text { color: blue }

p.large-text { font-size: 20px }
.small-text { font-size: 12px }
```

The blue text is inlined because its class comes *after* the red class in app.css  
But the large text gets inlined because it has greater specificity than the small text class

```html
<p class="blue-text red-text large-text small-text" styles="color:blue; font-size: 20px">Pika pika!</p>
```

The order you write classes into `<p>` doesn't matter.